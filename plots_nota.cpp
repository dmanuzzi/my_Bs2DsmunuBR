#include <iostream>
#include "TChain.h"
#include "TString.h"
#include "/afs/cern.ch/user/d/dmanuzzi/Bs2Dmunu/Bs2DsBrRatio/analysis/tools/Tools.h"
#include <TLorentzVector.h>
#include "TH1D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TStyle.h"
#include "TLegend.h"
using namespace std;

int plots_nota(){
  TString tag;
  const int Nmax = 1000;
  TChain *Data_KKpi = new TChain("DMutuple/DMutuple");
  TString pathMelody = "/eos/lhcb/user/m/mravonel/SLB/Datarun2/";
  Data_KKpi->Add(pathMelody+"2015Dw_KKpi/B2DmuNtuple_*.root");
  // Data_KKpi->Add(pathMelody+"2015Up_KKpi/B2DmuNtuple_*.root", Nmax);
  Data_KKpi->Add(pathMelody+"2016Dw_KKpi/B2DmuNtuple_*.root");
  // Data_KKpi->Add(pathMelody+"2016Up_KKpi/B2DmuNtuple_*.root", Nmax);
  TChain *Data_KKpi_SS = new TChain("SSDMutuple/SSDMutuple");
  Data_KKpi_SS->Add(pathMelody+"2015Dw_KKpi/B2DmuNtuple_*.root");
  // Data_KKpi_SS->Add(pathMelody+"2015Up_KKpi/B2DmuNtuple_*.root", Nmax);
  Data_KKpi_SS->Add(pathMelody+"2016Dw_KKpi/B2DmuNtuple_*.root");
  // Data_KKpi_SS->Add(pathMelody+"2016Up_KKpi/B2DmuNtuple_*.root", Nmax);

  TChain *Data_Kpipi = new TChain("DMutuple/DMutuple");
  Data_Kpipi->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2015/Up/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2015/Down/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2016/Up/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2016/Down/B2DKpipimuNtuple*.root", Nmax);
  TChain *Data_Kpipi_SS = new TChain("SSDMutuple/SSDMutuple");
  Data_Kpipi_SS->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2015/Up/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi_SS->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2015/Down/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi_SS->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2016/Up/B2DKpipimuNtuple*.root", Nmax);
  // Data_Kpipi_SS->Add("/eos/lhcb/user/f/fferrari/TuplesSL/B2DKpipimu/2016/Down/B2DKpipimuNtuple*.root", Nmax);


  TFile *fout =new TFile("note_histo.root", "RECREATE");
 
  gStyle->SetOptStat(0);


  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "D_M-KKpi";
  cout << "==> Now plotting " << tag << endl; 
  TH1D *h_D_M_KKpi = new TH1D("h_"+tag, "h_"+tag, 200, 1800, 2030);
  Data_KKpi->Draw("D_M>>h_"+tag, "1" , "goff");
  TH1D *h_D_M_KKpi_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 1800, 2030);
  Data_KKpi_SS->Draw("D_M>>h_"+tag+"_SS", "1", "goff");
  TCanvas *c1 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  h_D_M_KKpi->SetLineColor(4);
  h_D_M_KKpi->GetXaxis()->SetTitle("m(KK#pi) MeV");
  h_D_M_KKpi->GetXaxis()->SetTitleOffset(1.3);
  h_D_M_KKpi->GetYaxis()->SetTitle("Candidates");
  h_D_M_KKpi->GetYaxis()->SetTitleOffset(1.4);
  h_D_M_KKpi->SetTitle("D_{(s)} mass (KK#pi)");
  h_D_M_KKpi->Draw();
  h_D_M_KKpi_SS->SetLineColor(2);
  h_D_M_KKpi_SS->Draw("same");
  TLegend *leg1 = new TLegend(0.15,0.7,0.4,0.9);
  leg1->AddEntry(h_D_M_KKpi,    "Data", "l");
  leg1->AddEntry(h_D_M_KKpi_SS, "SS Data", "l");
  leg1->Draw();
  c1->SaveAs("can_"+tag+".png");
  c1->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c1, c1->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------

  tag = "D_M-Kpipi";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_D_M_Kpipi = new TH1D("h_"+tag, "h_"+tag, 200, 1800, 1950);
  Data_Kpipi->Draw("D_M>>h_"+tag, "1" , "goff");
  TH1D *h_D_M_Kpipi_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 1800, 1950);
  Data_Kpipi_SS->Draw("D_M>>h_"+tag+"_SS", "1", "goff");
  TCanvas *c2 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  h_D_M_Kpipi->SetLineColor(4);
  h_D_M_Kpipi->GetXaxis()->SetTitle("m(K#pi#pi) MeV");
  h_D_M_Kpipi->GetXaxis()->SetTitleOffset(1.3);
  h_D_M_Kpipi->GetYaxis()->SetTitle("Candidates");
  h_D_M_Kpipi->GetYaxis()->SetTitleOffset(1.4);
  h_D_M_Kpipi->SetTitle("D mass (K#pi#pi)");
  h_D_M_Kpipi->Draw();
  h_D_M_Kpipi_SS->SetLineColor(2);
  h_D_M_Kpipi_SS->Draw("same");
  TLegend *leg2 = new TLegend(0.15,0.7,0.4,0.9);
  leg2->AddEntry(h_D_M_Kpipi,    "Data", "l");
  leg2->AddEntry(h_D_M_Kpipi_SS, "SS Data", "l");
  leg2->Draw();
  c2->SaveAs("can_"+tag+".png");
  c2->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c2, c2->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------   
  //-------------------------------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_M-KKpi-D";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_M_KKpiD = new TH1D("h_"+tag, "h_"+tag, 200, 3000, 8500);
  Data_KKpi->Draw("B_M>>h_"+tag, "D_M>1850 && D_M < 1890" , "goff");
  TH1D *h_B_M_KKpiD_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 3000, 8500);
  Data_KKpi_SS->Draw("B_M>>h_"+tag+"_SS", "D_M>1850 && D_M < 1890", "goff");
  TCanvas *c3 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c3->SetLogy();
  h_B_M_KKpiD->SetLineColor(4);
  h_B_M_KKpiD->GetXaxis()->SetTitle("m(KK#pi#mu) MeV");
  h_B_M_KKpiD->GetXaxis()->SetTitleOffset(1.3);
  h_B_M_KKpiD->GetYaxis()->SetTitle("Candidates");
  h_B_M_KKpiD->GetYaxis()->SetTitleOffset(1.4);
  h_B_M_KKpiD->SetTitle("B mass (KK#pi)");
  h_B_M_KKpiD->Draw();
  h_B_M_KKpiD_SS->SetLineColor(2);
  h_B_M_KKpiD_SS->Draw("same");
  TLegend *leg3 = new TLegend(0.65,0.7,0.9,0.9);
  leg3->AddEntry(h_B_M_KKpiD,    "Data", "l");
  leg3->AddEntry(h_B_M_KKpiD_SS, "SS Data", "l");
  leg3->Draw();
  c3->SaveAs("can_"+tag+".png");
  c3->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c3, c3->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_M-KKpi-Ds";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_M_KKpiDs = new TH1D("h_"+tag, "h_"+tag, 200, 3000, 8500);
  Data_KKpi->Draw("B_M>>h_"+tag, "D_M>1940 && D_M < 2100" , "goff");
  TH1D *h_B_M_KKpiDs_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 3000, 8500);
  Data_KKpi_SS->Draw("B_M>>h_"+tag+"_SS", "D_M>1940 && D_M < 2100", "goff");
  TCanvas *c31 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c31->SetLogy();
  h_B_M_KKpiDs->SetLineColor(4);
  h_B_M_KKpiDs->GetXaxis()->SetTitle("m(KK#pi#mu) MeV");
  h_B_M_KKpiDs->GetXaxis()->SetTitleOffset(1.3);
  h_B_M_KKpiDs->GetYaxis()->SetTitle("Candidates");
  h_B_M_KKpiDs->GetYaxis()->SetTitleOffset(1.4);
  h_B_M_KKpiDs->SetTitle("B_{s} mass (KK#pi)");
  h_B_M_KKpiDs->Draw();
  h_B_M_KKpiDs_SS->SetLineColor(2);
  h_B_M_KKpiDs_SS->Draw("same");
  TLegend *leg31 = new TLegend(0.65,0.7,0.9,0.9);
  leg31->AddEntry(h_B_M_KKpiDs,    "Data", "l");
  leg31->AddEntry(h_B_M_KKpiDs_SS, "SS Data", "l");
  leg31->Draw();
  c31->SaveAs("can_"+tag+".png");
  c31->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c31, c31->GetName(), "Overwrite");

  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_M-Kpipi";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_M_Kpipi = new TH1D("h_"+tag, "h_"+tag, 200, 2200, 8000);
  Data_Kpipi->Draw("B_M>>h_"+tag,  "D_M>1850 && D_M < 1890" , "goff");
  TH1D *h_B_M_Kpipi_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 2200, 8000);
  Data_Kpipi_SS->Draw("B_M>>h_"+tag+"_SS", "D_M>1850 && D_M < 1890", "goff");
  TCanvas *c4 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c4->SetLogy();
  h_B_M_Kpipi->SetLineColor(4);
  h_B_M_Kpipi->GetXaxis()->SetTitle("m(K#pi#pi#mu) MeV");
  h_B_M_Kpipi->GetXaxis()->SetTitleOffset(1.3);
  h_B_M_Kpipi->GetYaxis()->SetTitle("Candidates");
  h_B_M_Kpipi->GetYaxis()->SetTitleOffset(1.4);
  h_B_M_Kpipi->SetTitle("B mass (K#pi#pi)");
  h_B_M_Kpipi->Draw();
  h_B_M_Kpipi_SS->SetLineColor(2);
  h_B_M_Kpipi_SS->Draw("same");
  TLegend *leg4 = new TLegend(0.65,0.7,0.9,0.9);
  leg4->AddEntry(h_B_M_Kpipi,    "Data", "l");
  leg4->AddEntry(h_B_M_Kpipi_SS, "SS Data", "l");
  leg4->Draw();
  c4->SaveAs("can_"+tag+".png");
  c4->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c4, c4->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_MCORR-KKpi-D";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_MCORR_KKpiD = new TH1D("h_"+tag, "h_"+tag, 200, 3000, 8500);
  Data_KKpi->Draw("B_MCORR>>h_"+tag,  "D_M>1850 && D_M < 1890", "goff");
  TH1D *h_B_MCORR_KKpiD_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 3000, 8500);
  Data_KKpi_SS->Draw("B_MCORR>>h_"+tag+"_SS", "D_M>1850 && D_M < 1890", "goff");
  TCanvas *c5 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c5->SetLogy();
  h_B_MCORR_KKpiD->SetLineColor(4);
  h_B_MCORR_KKpiD->GetXaxis()->SetTitle("m_{corr} MeV");
  h_B_MCORR_KKpiD->GetXaxis()->SetTitleOffset(1.3);
  h_B_MCORR_KKpiD->GetYaxis()->SetTitle("Candidates");
  h_B_MCORR_KKpiD->GetYaxis()->SetTitleOffset(1.4);
  h_B_MCORR_KKpiD->SetTitle("Corrected B mass (KK#pi)");
  h_B_MCORR_KKpiD->Draw();
  h_B_MCORR_KKpiD_SS->SetLineColor(2);
  h_B_MCORR_KKpiD_SS->Draw("same");
  TLegend *leg5 = new TLegend(0.65,0.7,0.9,0.9);
  leg5->AddEntry(h_B_MCORR_KKpiD,    "Data", "l");
  leg5->AddEntry(h_B_MCORR_KKpiD_SS, "SS Data", "l");
  leg5->Draw();
  c5->SaveAs("can_"+tag+".png");
  c5->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c5, c5->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_MCORR-KKpi-Ds";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_MCORR_KKpiDs = new TH1D("h_"+tag, "h_"+tag, 200, 3000, 8500);
  Data_KKpi->Draw("B_MCORR>>h_"+tag, "D_M>1940 && D_M < 2100" , "goff");
  TH1D *h_B_MCORR_KKpiDs_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 3000, 8500);
  Data_KKpi_SS->Draw("B_MCORR>>h_"+tag+"_SS", "D_M>1940 && D_M < 2100", "goff");
  TCanvas *c51 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c51->SetLogy();
  h_B_MCORR_KKpiDs->SetLineColor(4);
  h_B_MCORR_KKpiDs->GetXaxis()->SetTitle("m_{corr} MeV");
  h_B_MCORR_KKpiDs->GetXaxis()->SetTitleOffset(1.3);
  h_B_MCORR_KKpiDs->GetYaxis()->SetTitle("Candidates");
  h_B_MCORR_KKpiDs->GetYaxis()->SetTitleOffset(1.4);
  h_B_MCORR_KKpiDs->SetTitle("Corrected B_{s} mass (KK#pi)");
  h_B_MCORR_KKpiDs->Draw();
  h_B_MCORR_KKpiDs_SS->SetLineColor(2);
  h_B_MCORR_KKpiDs_SS->Draw("same");
  TLegend *leg51 = new TLegend(0.65,0.7,0.9,0.9);
  leg51->AddEntry(h_B_MCORR_KKpiDs,    "Data", "l");
  leg51->AddEntry(h_B_MCORR_KKpiDs_SS, "SS Data", "l");
  leg51->Draw();
  c51->SaveAs("can_"+tag+".png");
  c51->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c51, c51->GetName(), "Overwrite");

  //-------------------------------------------------------------------------------------------------------------------------------------
  tag = "B_MCORR-Kpipi";
  cout << "==> Now plotting " << tag << endl;
  TH1D *h_B_MCORR_Kpipi = new TH1D("h_"+tag, "h_"+tag, 200, 2200, 8000);
  Data_Kpipi->Draw("B_MCORR>>h_"+tag, "D_M>1850 && D_M < 1890" , "goff");
  TH1D *h_B_MCORR_Kpipi_SS = new TH1D("h_"+tag+"_SS", "h_"+tag+"_SS", 200, 2200, 8000);
  Data_Kpipi_SS->Draw("B_MCORR>>h_"+tag+"_SS", "D_M>1850 && D_M < 1890", "goff");
  TCanvas *c6 = new TCanvas("can_"+tag, "can_"+tag, 800,600);
  c6->SetLogy();
  h_B_MCORR_Kpipi->SetLineColor(4);
  h_B_MCORR_Kpipi->GetXaxis()->SetTitle("m_{corr} MeV");
  h_B_MCORR_Kpipi->GetXaxis()->SetTitleOffset(1.3);
  h_B_MCORR_Kpipi->GetYaxis()->SetTitle("Candidates");
  h_B_MCORR_Kpipi->GetYaxis()->SetTitleOffset(1.4);
  h_B_MCORR_Kpipi->SetTitle("Corrected B mass (K#pi#pi)");
  h_B_MCORR_Kpipi->Draw();
  h_B_MCORR_Kpipi_SS->SetLineColor(2);
  h_B_MCORR_Kpipi_SS->Draw("same");
  TLegend *leg6 = new TLegend(0.65,0.7,0.9,0.9);
  leg6->AddEntry(h_B_MCORR_Kpipi,    "Data", "l");
  leg6->AddEntry(h_B_MCORR_Kpipi_SS, "SS Data", "l");
  leg6->Draw();
  c6->SaveAs("can_"+tag+".png");
  c6->SaveAs("can_"+tag+".pdf");
  fout->WriteTObject(c6, c6->GetName(), "Overwrite");
  //-------------------------------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------------------


  



  //  fout->Close();
  
  return 1;
}
